package oleg_bot

import (
	"errors"
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var (
	ERR_EMPTY_TOKEN         = errors.New("Provided bot token is empty")
	ERR_EMPTY_PAYMENT_TOKEN = errors.New("Provided payment token is empty")
)

type BotInfo struct {
	Token        string
	PaymentToken string
}

type BotService interface {
	Run() (Done <-chan error)
}

type botSession struct {
}

type botContext struct {
	Info              BotInfo
	Sessions          map[UID]botSession
	Api               *tgbotapi.BotAPI
	paymentManager    PaymentManager
	userSessions      map[UID]botSession
	commandDispatcher CommandDispatcher
	userRepo UserRepository
	roleRepo RoleRepository
}

func (self *botContext) SendErrorMessage(chatId int64, user UID, errMsg string) error {
	userData, err := self.userRepo.GetUser(user)
	if err != nil {
		return err
	}
	if userData != nil && userData.Role.HasPermission(PERMISSION_DEVELOPER) {
		msg := tgbotapi.NewMessage(chatId, "Internal error: " + errMsg)
		_, err = self.Api.Send(msg)
		return err
	} else {
		msg := tgbotapi.NewMessage(chatId, "Sorry, an internal error ocurred!")
		log.Println(errMsg)
		_, err = self.Api.Send(msg)
		return err
	}
}

func (self *botContext) Run() (Done <-chan error) {
	c := make(chan error)

	go func() {
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates := self.Api.GetUpdatesChan(u)
		for update := range updates {
			if update.Message != nil {
				uid := UID(update.Message.From.ID)
				chatId := update.Message.Chat.ID
				log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
				if update.Message.Text != "" {
					err := self.commandDispatcher.Dispatch(update.Message)
					if err != nil {
						if err != nil && err.code == ERR_MESSAGE_IS_NOT_COMMAND {
							msg := tgbotapi.NewMessage(chatId, "This message is not a command.")
							_, e := self.Api.Send(msg)
							if e != nil {
								log.Println(e)
							}		
						} else if err != nil && err.code == ERR_COMMAND_NOT_FOUND {
							msg := tgbotapi.NewMessage(chatId, "I don't know this command.")
							_, e := self.Api.Send(msg)
							if e != nil {
								log.Println(e)
							}		
						} else {
							self.SendErrorMessage(chatId, uid, err.Error())
						}
					}

				} else if update.Message.SuccessfulPayment != nil {
					session, err := self.paymentManager.GetSession(uid)
					if err != nil {
						self.SendErrorMessage(chatId, uid, err.Error())
						break
					}
					err = session.ProcessPaymentResult()
					if err != nil {
						self.SendErrorMessage(chatId, uid, err.Error())
						break
					}
					msg := tgbotapi.NewMessage(chatId, "Success!")
					_, e := self.Api.Send(msg)
					if e != nil {
						log.Println(e)
					}	
				}
			} else if update.PreCheckoutQuery != nil {
				session, err := self.paymentManager.OpenSession(UID(update.PreCheckoutQuery.From.ID))
				if err == nil && session != nil {
					err = session.ProcessPreCheckout(FeeInfo{update.PreCheckoutQuery.Currency, uint64(update.PreCheckoutQuery.TotalAmount)})
				}
				config := tgbotapi.PreCheckoutConfig{ PreCheckoutQueryID: update.PreCheckoutQuery.ID }
				if err == nil {
					config.OK = true
				} else {
					config.OK = false
					config.ErrorMessage = err.Error()
				}
				_, e := self.Api.Send(config)
				if e != nil {
					log.Println(e)
				}
			}
		}
	}()

	return c
}

func NewBot(info BotInfo) (BotService, error) {
	paymentManager := NewPaymentManager()
	userRepo := NewInMemoryUserRepository()
	roleRepo := NewInMemoryRoleRepository()
	commandDispatcher := NewDispatcher()

	if len(info.Token) == 0 {
		return nil, ERR_EMPTY_TOKEN
	}
	if len(info.PaymentToken) == 0 {
		return nil, ERR_EMPTY_PAYMENT_TOKEN
	}
	bot, err := tgbotapi.NewBotAPI(info.Token)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error initializing Bot API: %s", err.Error()))
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	commandDispatcher.AddCommand("/start", NewStartCommand(bot, userRepo))
	commandDispatcher.AddCommand("/fee", NewPayFeeCommand(bot, paymentManager, userRepo, info.PaymentToken))
	commandDispatcher.AddCommand("/register", NewRegisterUserCommand(bot, userRepo, roleRepo))

	return &botContext{
		Info: info, 
		Api: bot,
		Sessions: make(map[UID]botSession), 
		commandDispatcher: commandDispatcher, 
		paymentManager: paymentManager,
		userRepo: userRepo,
		roleRepo: roleRepo,
	}, nil
}
