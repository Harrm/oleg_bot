package oleg_bot

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	ERR_DUPLICATE_USER_SESSION uint8 = iota
	ERR_USER_SESSION_NOT_FOUND uint8 = iota
)

type PaymentError struct {
	code uint8
	err_msg  string
}

func (err *PaymentError) Error() string {
	return err.err_msg
}

type FeeInfo struct {
	Currency string
	Amount uint64
}

type PaymentSession interface {
	ProcessPreCheckout(fee FeeInfo) *PaymentError
	ProcessPaymentResult() *PaymentError
}

type paymentSession struct {
	bot *tgbotapi.BotAPI
}

func (self *paymentSession) ProcessPreCheckout(fee FeeInfo) *PaymentError {
	return nil
}

func (self *paymentSession) ProcessPaymentResult() *PaymentError {
	return nil
}

type PaymentManager interface {
	OpenSession(user UID) (PaymentSession, *PaymentError)
	GetSession(user UID) (PaymentSession, *PaymentError)
}

type paymentManager struct {
	sessions map[UID]PaymentSession
}

func NewPaymentManager() PaymentManager {
	return &paymentManager{sessions: map[UID]PaymentSession{}}
}

func (self *paymentManager) OpenSession(user UID) (PaymentSession, *PaymentError) {
	_, ok := self.sessions[user]
	if ok {
		return nil, &PaymentError{ERR_DUPLICATE_USER_SESSION, fmt.Sprintf("Payment session for user %d is already open", user)}
	}
	session := paymentSession {}
	self.sessions[user] = &session

	return &session, nil
}

func (self *paymentManager) GetSession(user UID) (PaymentSession, *PaymentError) {
	session, ok := self.sessions[user]
	if !ok {
		return nil, &PaymentError{ERR_USER_SESSION_NOT_FOUND, fmt.Sprintf("Payment session for user %d is not found", user)}
	}
	return session, nil
}
