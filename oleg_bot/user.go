package oleg_bot

import (
	"errors"
	"fmt"
)

type UID uint64

type Permission uint8

const (
	PERMISSION_ADD_USER    Permission = iota
	PERMISSION_REMOVE_USER            = iota
	PERMISSION_GRANT_ROLE             = iota
	PERMISSION_READ_LEDGER            = iota
	PERMISSION_DEVELOPER              = iota
)

type UserRole interface {
	HasPermission(permission Permission) bool
}

type userRoleData struct {
	permissions map[Permission]bool
}

func (role *userRoleData) HasPermission(permission Permission) bool {
	return role.permissions[permission]
}

type UserData struct {
	Role  UserRole
	Name  string
	Alias string
}

type UserDataChange struct {
	Role  UserRole
	Name  *string
	Alias *string
}

func (user *UserData) HasPermission(permission Permission) bool {
	return user.Role.HasPermission(permission)
}

type UserRepository interface {
	AddUser(user UID, data UserData) error
	RemoveUser(user UID) error
	ModifyUser(user UID, data UserDataChange) error
	GetUser(user UID) (*UserData, error)
	FindUserByAlias(alias string) (*UserData, error)
}

type RoleRepository interface {
	AddRole(name string, permissions []Permission) error
	RemoveRole(name string) error
	GetRole(name string) (UserRole, error)
	GetDefaultRole() (UserRole, error)
	SetDefaultRole(name string) error
}

type inMemoryUserRepository struct {
	users map[UID]UserData
}

func (self *inMemoryUserRepository) AddUser(user UID, data UserData) error {
	_, ok := self.users[user]
	if ok {
		return errors.New(fmt.Sprintf("Adding a duplicate user '%s'", user))
	}
	self.users[user] = data
	return nil
}

func (self *inMemoryUserRepository) RemoveUser(user UID) error {
	return errors.New("Not implemented")
}

func (self *inMemoryUserRepository) ModifyUser(user UID, data UserDataChange) error {
	return errors.New("Not implemented")
}

func (self *inMemoryUserRepository) GetUser(user UID) (*UserData, error) {
	user_data, ok := self.users[user]
	if ok {
		return &user_data, nil
	}
	return nil, nil
}

func (self *inMemoryUserRepository) FindUserByAlias(alias string) (*UserData, error) {
	for _, user_data := range self.users {
		if user_data.Alias == alias {
			return &user_data, nil
		}
	}
	return nil, nil
}

type inMemoryRoleRepository struct {
	roles       map[string]userRoleData
	defaultRole string
}

func (self *inMemoryRoleRepository) AddRole(name string, permissions []Permission) error {
	_, ok := self.roles[name]
	if ok {
		return errors.New(fmt.Sprintf("Adding a duplicate role '%s'", name))
	}
	role := userRoleData{}
	for _, permission := range permissions {
		role.permissions[permission] = true
	}
	self.roles[name] = role
	return nil
}

func (self *inMemoryRoleRepository) RemoveRole(name string) error {
	return errors.New("Not implemented")
}

func (self *inMemoryRoleRepository) GetRole(name string) (UserRole, error) {
	user_data, ok := self.roles[name]
	if ok {
		return &user_data, nil
	}
	return nil, nil
}

func (self *inMemoryRoleRepository) GetDefaultRole() (UserRole, error) {
	return self.GetRole(self.defaultRole)
}

func (self *inMemoryRoleRepository) SetDefaultRole(name string) error {
	_, ok := self.roles[name]
	if !ok {
		return errors.New(fmt.Sprintf("Cannot make '%s' the default role, as this role doesn't exist", name))
	}
	self.defaultRole = name
	return nil
}

func NewInMemoryUserRepository() UserRepository {
	return &inMemoryUserRepository{map[UID]UserData{}}
}

func NewInMemoryRoleRepository() RoleRepository {
	return &inMemoryRoleRepository{map[string]userRoleData{"slave": userRoleData{map[Permission]bool{}}}, "slave"}
}
