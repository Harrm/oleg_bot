package oleg_bot

import (
	"fmt"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type botCommand struct {
	bot *tgbotapi.BotAPI
}

func (self *botCommand) ReportIfFailed(optErr *error, msg string) {

}

func (self *botCommand) reportErrorToUser(user UID, msg string) error {
	report := tgbotapi.NewMessage(int64(user), "Error:" + msg)
	_, err := self.bot.Send(report)
	return err
}

type payFeeCommand struct {
	botCommand
	manager        PaymentManager
	userRepository UserRepository
	paymentToken   string
}

func NewPayFeeCommand(bot *tgbotapi.BotAPI, paymentManager PaymentManager, userRepository UserRepository, paymentToken string) Command {
	return &payFeeCommand{botCommand: botCommand{bot}, manager: paymentManager, paymentToken: paymentToken, userRepository: userRepository}
}

func (cmd *payFeeCommand) Execute(msg *tgbotapi.Message) error {
	user, err := cmd.userRepository.GetUser(UID(msg.From.ID))
	if err != nil {
		return err
	}
	if user == nil {
		//TODO: probably should try several times if failed
		reply := tgbotapi.NewMessage(msg.From.ID, "Sorry, you are not a registered member and cannot pay fee.")
		_, err := cmd.bot.Send(reply)
		return err
	}
	payload := fmt.Sprintf("uid:%d", msg.From.ID)
	invoice := tgbotapi.NewInvoice(msg.Chat.ID, "Something", "Very good", payload, cmd.paymentToken, "1000", "RUB", []tgbotapi.LabeledPrice{{Label: "123", Amount: 12300}})
	invoice.SuggestedTipAmounts = []int{10000, 20000, 30000}
	invoice.MaxTipAmount = 30000
	invoice.ReplyToMessageID = msg.MessageID

	_, err = cmd.bot.Send(invoice)

	return err
}

type startCommand struct {
	botCommand
	userRepository UserRepository
}

func (cmd *startCommand) Execute(msg *tgbotapi.Message) error {
	reply := tgbotapi.NewMessage(msg.From.ID, "Hello!")
	user, err := cmd.userRepository.GetUser(UID(msg.From.ID))
	if err != nil {
		return err
	}
	_, err = cmd.bot.Send(reply)
	if err != nil {
		return err
	}
	var role_info tgbotapi.MessageConfig
	if user == nil {
		role_info = tgbotapi.NewMessage(msg.From.ID, "You are not a registered member and are limited in permitted operations.")
	} else {
		role_info = tgbotapi.NewMessage(msg.From.ID, "I know you, you are a registered member.")
	}
	//TODO: probably should try several times if failed
	_, err = cmd.bot.Send(role_info)
	return err
}

func NewStartCommand(bot *tgbotapi.BotAPI, userRepository UserRepository) Command {
	return &startCommand{botCommand: botCommand{bot}, userRepository: userRepository}
}

type registerUserCommand struct {
	botCommand
	userRepository UserRepository
	roleRepository RoleRepository
}

func (cmd *registerUserCommand) makeUserFromTgUser(user *tgbotapi.User) (*UserData, error) {
	fullname := ""
	if user.FirstName != "" {
		fullname += user.FirstName
	}
	if user.LastName != "" {
		fullname += " " + user.LastName
	}
	defaultRole, err := cmd.roleRepository.GetDefaultRole()
	if err != nil {
		return nil, err
	}
	return &UserData{ Alias: user.UserName, Name: fullname, Role: defaultRole }, nil
}

func (cmd *registerUserCommand) Execute(msg *tgbotapi.Message) error {
	words := strings.Split(msg.Text, " ")
	if len(words) != 2 {
		return cmd.reportErrorToUser(UID(msg.From.ID), "Usage: /register @alias")
	}
	if strings.HasPrefix(words[1], "@") {
		user, err := cmd.userRepository.FindUserByAlias(words[1])
		if err != nil {
			return err
		}
		if user != nil {
			return cmd.reportErrorToUser(UID(msg.From.ID), "User is already registered!")
		}
	}
	user, err := cmd.makeUserFromTgUser(msg.From)
	if err != nil {
		return err
	}

	err = cmd.userRepository.AddUser(UID(msg.From.ID), *user)
	if err == nil {
		reply := tgbotapi.NewMessage(msg.From.ID, "Successfully registered a user.")
		_, err = cmd.bot.Send(reply)
		return err
	}
	return err
}

func NewRegisterUserCommand(bot *tgbotapi.BotAPI, userRepository UserRepository, roleRepository RoleRepository) Command {
	return &registerUserCommand{botCommand: botCommand{bot}, userRepository: userRepository, roleRepository: roleRepository}
}
