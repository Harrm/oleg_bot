// Command dispatcher for commands which a user sends in text messages
package oleg_bot

import (
	"fmt"
	"strings"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	ERR_DUPLICATE_COMMAND uint8 = iota
	ERR_COMMAND_NOT_FOUND uint8 = iota
	ERR_MESSAGE_IS_NOT_COMMAND uint8 = iota
	ERR_COMMAND_EXECUTION_ERROR uint8 = iota
)

type DispatcherError struct {
	code uint8
	err_msg string
}

func (err DispatcherError) Error() string {
	return err.err_msg
}

func newErrDuplicateCommand(name string) *DispatcherError {
	return &DispatcherError{ERR_DUPLICATE_COMMAND, fmt.Sprintf("Error: attempt to add a duplicate command '%s'", name)}
}

func newErrCommandNotFound(name string) *DispatcherError {
	return &DispatcherError{ERR_COMMAND_NOT_FOUND, fmt.Sprintf("Error: dispatching a command not known to the dispatcher '%s'", name)}
}

func newErrMsgIsNotCmd(msg string) *DispatcherError {
	return &DispatcherError{ERR_MESSAGE_IS_NOT_COMMAND, fmt.Sprintf("Error: provided message is not a command: '%s'", msg)}
}

func newErrCmdExecutionError(cmd string, err error) *DispatcherError {
	return &DispatcherError{ERR_COMMAND_EXECUTION_ERROR, fmt.Sprintf("Error: failed to execute command '%s': '%s'", cmd, err.Error())}
}


type Command interface {
	Execute(msg *tgbotapi.Message) error
}

type CommandDispatcher interface {
	AddCommand(name string, command Command) *DispatcherError

	Dispatch(msg *tgbotapi.Message) *DispatcherError
}

type commandDispatcher struct {
	commands map[string]Command
}

func (self *commandDispatcher) AddCommand(name string, command Command) *DispatcherError {
	_, ok := self.commands[name]
	if ok {
		return newErrDuplicateCommand(name)
	}
	self.commands[name] = command
	return nil
}

func (self *commandDispatcher) Dispatch(msg *tgbotapi.Message) *DispatcherError {
	if !strings.HasPrefix(msg.Text, "/") {
		return newErrMsgIsNotCmd(msg.Text)
	}
	split_msg := strings.SplitN(msg.Text, " ", 2)
	name := split_msg[0]
	cmd, ok := self.commands[name]
	if !ok {
		return newErrCommandNotFound(name)
	}
	err := cmd.Execute(msg)
	if err != nil {
		return newErrCmdExecutionError(name, err)
	}
	return nil
}

func NewDispatcher() CommandDispatcher {
	return &commandDispatcher{commands: map[string]Command{}}
}
