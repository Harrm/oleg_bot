package main

import (
	"os"
	"log"
	"oleg_bot/oleg_bot"
)

func main() {
	token := os.Getenv("OLEG_BOT_TOKEN")
	paymentToken := os.Getenv("OLEG_BOT_SBERBANK_TEST_PAYMENT_TOKEN")

	botService, err := oleg_bot.NewBot(oleg_bot.BotInfo { Token: token, PaymentToken: paymentToken })
	if err != nil {
		log.Panic(err.Error())
	}
	done := botService.Run()
	botError := <-done
	if botError != nil {
		log.Panic(botError.Error())
	}
}
