module oleg_bot

go 1.17

require github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1

require (
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/tools v0.1.8 // indirect
)
